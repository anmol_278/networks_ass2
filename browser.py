import object_downloader
from collections import deque
import urlparse
import threading
import json
class Browser:

	def __init__(self,**kwargs):
		self.MAX_CONN_PER_DOMAIN = kwargs['max_conn_per_domain']
		self.MAX_OBJECTS_PER_CONN = kwargs['max_objects_per_conn']
		self.connections = {}
		self.lock = threading.LOCK()
		self.frontier = deque()
	def on_download_complete(self,download_obj):
		self.lock = threading.Lock()
		self.frontier = deque()
	def on_download_complete(self,download_obj):
		print 'downloaded {}'.format(download_obj['url'])
		self.lock.acquire()
		for child in download_obj['children']:
			self.frontier.append(self.request_tree[child])
		self.lock.release()

	def start_download(self,request_tree):
		self.request_tree = request_tree 	#python allows dynamic defining of members
		self.frontier.append(request_tree[0])	#no locking needed here

		while True:		#TODO replace with something else, which would be false when all objects have been downloaded. maybe the count of objects
			if len(self.frontier) > 0:
				self.lock.acquire()
				curr_obj = self.frontier.popleft()
				self.lock.release()

				domain = urlparse(curr_obj['url']).netloc
				if domain not in self.connections :
				domain = urlparse.urlparse(curr_obj['url']).netloc
				if domain not in self.connections :
					print 'adding connections to {}'.format(domain)
					self.connections.update( {
					 domain : [ object_downloader.ObjectDownloader(
					 	domain=domain,
					 	root_dir='./resources',
						max_objects_per_conn=self.MAX_OBJECTS_PER_CONN
						) for _ in xrange(self.MAX_CONN_PER_DOMAIN) ] 
					 } )

				#find the downloader with minimum object downloads. Possible to use some other measure to distribute the download jobs
				selected_downloader = min(connections,key=lambda x: x.object_downloads_started)
				
				selected_downloader.queue_download(curr_obj['url'], lambda _: self.on_download_complete(curr_obj) ) #currying at its best

if __name__ == '__main__':
	print 'shubham rawat is a stud'
					for downloader in self.connections[domain]:
						downloader.start()

				#find the downloader with minimum object downloads. Possible to use some other measure to distribute the download jobs
				selected_downloader = min(self.connections[domain],key=lambda x: x.object_downloads_started)
				
				selected_downloader.queue_download(curr_obj['url'], lambda : self.on_download_complete(curr_obj) ) #currying at its best

if __name__ == '__main__':
	request_tree = [
	{	
		'url':'http://cse.iitd.ac.in/~mausam/courses/csl333/autumn2015/',
		'children':[1]

	},
	{	
		'url':'http://cse.iitd.ac.in/~mausam/courses/csl333/autumn2015/lectures/01-intro.pdf',
		'children':[]
	}
	]
	rtree = open('rtee.json','r').read()
	request_tree = json.loads(rtree)
	chrome = Browser(max_conn_per_domain=2,max_objects_per_conn=5)
	try:
		chrome.start_download(request_tree)
	except Exception as e:
		print e
