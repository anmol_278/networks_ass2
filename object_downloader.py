import socket
import threading
import sys
from collections import deque
import urlparse
import os
import signal
CLRF = '\r\n\r\n'

class ObjectDownloader(threading.Thread):
	def __init__(self,**kwargs):
		threading.Thread.__init__(self)
		try:
			self.domain = kwargs['domain']
			self.root_dir = kwargs['root_dir']
			self.MAX_OBJECTS_PER_CONN = kwargs['max_objects_per_conn']
		except KeyError:
			print ("Invalid initialization of ObjectDownloader, aborting")
			sys.exit(1)

		self.object_downloads_completed = 0
		self.object_downloads_started = 0
		self.queued_downloads = deque()
		self.socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
		self._stop = False
		self.conditional_var = threading.Condition()	#put the thread to sleep until it has a job in the queue
		self.queue_lock = threading.Lock()
		self.connected = False
	def recreate_socket(self):
		self.socket.close()
		self.connected = False
		self.socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
		self.object_downloads_completed = 0
		self.object_downloads_started = 0

	def initiate_connection(self,port=80):
		try:
			self.socket.connect((self.domain,port))
		except Exception as e:

			print "failed to connect to {domain}:{port}.\n{e}".format(domain=self.domain,port=port,e=e)
			return False
		self.connected = True
		return True
	def download(self,url):
		#resource_name =	url.split('/')[-1]
		original_url = url
		url = urlparse.urlparse(url)
		path = url.path
		if(path == ''): 
			path = "/"
		#print url.netloc,self.domain

		print url.netloc,self.domain
		assert url.netloc == self.domain
		
		if not self.connected:
			tries = 5
			while tries > 0 and not self.connected:
				self.initiate_connection()
				tries -= 1
			if not self.connected : return False 
		send_ret = self.socket.send('GET {} HTTP/1.0{}'.format(path,CLRF))
		#print 'sent {}'.format(send_ret)
			if not self.connected : return False
		self.socket.send('GET {} HTTP/1.0\r\n'.format(path))
		self.socket.send('Host: {}\r\n'.format(self.domain))
		self.socket.send('Accept-Encoding: gzip, deflate, sdch\r\n\r\n')

		print 'sent {}'.format(path)
		data = ''
		chunk = ''
		count = 0
		while True:
			##print 'here'
			chunk = self.socket.recv(5120)
			##print (chunk+'...')
			if ( len(chunk) < 1 ) : break
			# time.sleep(0.25)
			count = count + len(chunk)
			data += chunk	
		#print data
		if not data:
			print 'server closed connection, recreating socket'
			self.recreate_socket()
			return self.download(original_url)
		else:
			header_end = data.find(CLRF)
			header = data[:header_end]
			content = data[header_end+4:]
			#print header
			path,resource = tuple(path.rsplit('/',1))
			#print path,resource
			#print content
			if(resource == ''):
				resource = 'index.html'		# is it ok?
			path = self.root_dir +'/' + self.domain + path + '/'
			if not os.path.exists(path):
				os.makedirs(path)
			f = open(path+resource,'wb')
			f.write(content)
			f.close()
			return True
			
	def have_job(self):
		return len(self.queued_downloads) > 0
	def queue_download(self,download_object,callback):
		self.queue_lock.acquire()
		self.queued_downloads.append((download_object,callback))
		self.queue_lock.release()
		self.conditional_var.acquire()
		self.conditional_var.notify()
		self.conditional_var.release()
		print 'queued_download : {}'.format(download_object)
	def reset(self):
		self.recreate_socket()
	def run(self):
		#print 'thread started'
		while not self._stop:
			self.conditional_var.acquire()
			while not self.have_job() and not self._stop: 
				print 'going to sleep'
				self.conditional_var.wait()
			self.conditional_var.release()
			if len(self.queued_downloads) > 0:
				print 'awake!'
				#print 'going to sleep'
				self.conditional_var.wait()
			self.conditional_var.release()
			if len(self.queued_downloads) > 0:
				#print 'awake!'

				self.queue_lock.acquire()
				curr_job,callback = self.queued_downloads.popleft()
				self.queue_lock.release()
				if self.object_downloads_started > self.MAX_OBJECTS_PER_CONN:
				if self.object_downloads_completed > self.MAX_OBJECTS_PER_CONN:
					self.recreate_socket()
				self.object_downloads_started += 1
				try:
					success = self.download(curr_job)
					if success:
						callback()
						#self.queue_lock.acquire()
						self.object_downloads_completed += 1
						#self.queue_lock.release()
				except Exception as e:
					print e
					break
					else:
						print 'failed to download (maximum tries exceeded) {}'.format(curr_job)

				except Exception as e:
					print e,'failed to download {}'.format(curr_job)
					#logger
		print 'thread for domain {} exiting'.format(self.domain)
	def stop(self):
		self._stop = True
		self.conditional_var.acquire()
		self.conditional_var.notify()
		self.conditional_var.release()
downloaders = []
def SIGINT_handler(signal,frame):
	print 'keyboard interrupt detected.... dying and taking my children with me :('
	for downloader in downloaders:
		downloader.stop()
	print "everyone dead.. look what you've made me do"
	#sys.exit(0)
if __name__ == '__main__':
	signal.signal(signal.SIGINT,SIGINT_handler)
	#print "OK"
	downloader = ObjectDownloader(domain='www.cse.iitd.ac.in',root_dir='./resources',max_objects_per_conn=5)
	downloader.start()
	downloaders.append(downloader)
	downloader.queue_download('http://www.cse.iitd.ac.in/~mausam/courses/csl333/autumn2015/')
	downloader.queue_download('http://www.cse.iitd.ac.in/~mausam/courses/csl333/autumn2015/lectures/01-intro.pdf')
	downloader = ObjectDownloader(domain='static01.nyt.com',root_dir='./resources',max_objects_per_conn=5)
	downloader.start()
	downloaders.append(downloader)
	downloader.queue_download('http://static01.nyt.com/images/2014/09/10/business/10STATE/10STATE-mediumSquare149.jpg',lambda : 1)
	#downloader.queue_download('http://cse.iitd.ac.in/~mausam/courses/csl333/autumn2015/lectures/01-intro.pdf', lambda : 1 )
	raw_input()
	for downloader in downloaders:
		downloader.stop()
	print 'exiting main thread'
	