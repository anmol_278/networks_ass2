import numpy as np
import matplotlib.pyplot as plt
import matplotlib.pyplot as plt1
import json
import pydot

def domainStats(filename):

	with open(filename) as data_file:    
	    data = json.load(data_file)

	parallel = []
	total = []
	size = []

	for x in data:
		parallel.append(data[x]['parallel'])
		total.append(data[x]['total'])
		size.append(data[x]['size'])

	N = len(parallel)

	ind = np.arange(N)  # the x locations for the groups
	width = 0.35       # the width of the bars

	fig, ax = plt.subplots()
	rects1 = ax.bar(ind, parallel, width, color='r')
	rects2 = ax.bar(ind+width, total, width, color='y')

	fig, axSize = plt.subplots()
	rects3 = axSize.bar(ind, size, width, color='r')

	ax.set_ylabel('Count')
	ax.set_xlabel('Domains')
	ax.set_title('Number of parallel and total TCP connections per domain')
	axSize.set_ylabel('Size (Bytes)')
	axSize.set_xlabel('Domains')
	axSize.set_title('Size of objects downloaded per per domain')

	ax.legend( (rects1[0], rects2[0]), ('parallel', 'total') )

	size.sort();
	print("Max Size Domains: ",size[len(size)-1],size[len(size)-2],size[len(size)-3])

def tcpStats(filename):
	with open(filename) as data_file:    
	    data = json.load(data_file)

	count = []
	size = []
	start = []
	end = []

	for i in range(0,len(data)):
		count.append(data[i]['count'])
		size.append(data[i]['size'])
		start.append(data[i]['start'])
		end.append(data[i]['end'])

	N = len(count)

	ind = np.arange(N)  # the x locations for the groups
	width = 0.35       # the width of the bars

	fig, axCount = plt.subplots()
	fig, axSize = plt.subplots()
	fig, axTimes = plt1.subplots()
	rects1 = axCount.bar(ind, count, width, color='r')
	rects2 = axSize.bar(ind, size, width, color='r')
	plt1.xlim([int(min(start)),int(max(end))])
	rects3 = axTimes.barh(ind, end, width, color='r',edgecolor = "none")
	rects4 = axTimes.barh(ind, start, width, color='w',edgecolor = "none")

	axCount.set_ylabel('Count')
	axSize.set_ylabel('Size (Bytes)')
	axTimes.set_ylabel('TCP Connection')
	axCount.set_xlabel('TCP Connection')
	axSize.set_xlabel('TCP Connection')
	axTimes.set_xlabel('Time (ms)')
	axCount.set_title('Total number of objects downloaded per TCP connection')
	axSize.set_title('Total size of objects downloaded per TCP connection')
	axTimes.set_title('Time spent per TCP connection')

	fig = plt.show()
	fig = plt1.show()

	size.sort()
	print("Max Size TCP connections: ",size[len(size)-1],size[len(size)-2],size[len(size)-3])

def draw(parent_name, child_name,graph):
    edge = pydot.Edge(parent_name, child_name)
    graph.add_edge(edge)

def myVisit(node,data,graph,parent=None):
	children = node['children']
	if(parent):
		draw(parent['index'],node['index'],graph)
	for x in children:
		myVisit(data[x],data,graph,node)

def graph(filename):
	with open(filename) as data_file:    
	    data = json.load(data_file)

	graph = pydot.Dot(graph_type='graph')
	myVisit(data[0],data,graph)
	graph.write_png('graph.png')


domainStats('domain_nytimes.har')
tcpStats('tcp_nytimes.har')
graph('rtee1.json')