# import socks
import socket
import time
import sys
import json
import urlparse
obj_tree = open(sys.argv[1])
# parallel_conn = int(sys.argv[2])
# objects_conn = int(sys.argv[3])
request_tree = []
parsed = json.load(obj_tree)
n_requests = len(parsed)

domains = []

#structure - list of dicts
# dict{key->domain name,[(a,x1),(b,x2),..]}
#x1,x2,... denote no. of objects downloaded in a connection
#a,b,.. are socket objects
#if any of xi=object_conn then remove this connection
#order of these connections shouldnt matter
existing_conn = []

#returns domain name corresponding to url
def domain(url):
	parsed_uri = urlparse.urlparse( url)
	domain = '{uri.netloc}'.format(uri=parsed_uri)
	return domain

#initialize request tree
for x in xrange(0,n_requests):
	d = {'url':parsed[x]['url'],'children':[]}
	request_tree.append(d)
request_tree[0]['height'] = 0;
# parse object tree to create tree structure 
for x in parsed:
	p = int(x['parent'])
	c = int(x['index'])
	if(p!=c):
		request_tree[p]['children'].append(c)
		request_tree[c]['height'] = request_tree[p]['height']+1

f = open('rtee.json','w')
json.dump(request_tree,f,indent=4)
f.close()
# for x in xrange(0,n_requests-1):
# 	print(x)
# 	for y in request_tree[x]['children']:
# 		print(y)
# 	print("done")
# print(request_tree[1]['url'])

# for x in request_tree:
# 	for y in x['children']:
# 		url_ = request_tree[y]['url']
# 		domain_= domain(url_)
# 		for z in existing_conn:
# 			if(z==domain_):
			#possible actions:
			#spawn a new connection
			#allot a connection to object
			#close a connection when saturated	

# mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# mysock.connect(('www.nytimes.com', 80))
# mysock.send('GET http://www.nytimes.com/ HTTP/1.0\n\n')


# count = 0
# picture = "";
# while True:
#     data = mysock.recv(5120)
#     if ( len(data) < 1 ) : break
#     # time.sleep(0.25)
#     count = count + len(data)
#     print (len(data))
#     print(count)
#     picture = picture + data

# mysock.close()

# # Look for the end of the header (2 CRLF)
# pos = picture.find("\r\n\r\n");
# print 'Header length',pos
# print picture[:pos]

# # Skip past the header and save the picture data
# picture = picture[pos+4:]
# fhand = open("stuff.txt","w")
# fhand.write(picture);
# fhand.close()