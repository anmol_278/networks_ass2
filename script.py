import json
import sys
from urllib.parse import urlparse
import re,datetime
import dateutil.parser
import pyshark

har = open(sys.argv[1])
tcp_map = []

def print_conversation_header(pkt):
 try:
  protocol = pkt.transport_layer
  src_port = pkt[pkt.transport_layer].srcport
  stream_id = pkt[pkt.transport_layer].stream
  if(protocol=="TCP"):
   try:
    if(pkt.highest_layer!="HTTP"):
     print("issues")
    index = len(pkt.layers)
    url_ = pkt[index-1].request_full_uri 
    c = {'stream' : stream_id,'src_p': src_port,'url':url_}
    tcp_map.append(c)
   except AttributeError as e:
    print("not found")
    pass   
 except AttributeError as e:
  #ignore packets that aren't TCP/UDP or IPv4
  pass

parsed = json.load(har)
entries  = parsed['log']['entries']
page = parsed['log']['pages']

#includes only successful requests
obj_down=0

total_size = 0
header_size = 0
body_size = 0

#returns domain name corresponding to url
def domain(url):
	parsed_uri = urlparse(url)
	domain = '{uri.netloc}'.format(uri=parsed_uri)
	return domain

def extractPath(url):
	parsed_uri = urlparse( url)
	path = '{uri.path}'.format(uri=parsed_uri)
	return path		

#object types 
content_types = dict([("javascript",0),("image",0),("json",0),("css",0),("htm",0),("xml",0),("zip",0),("text",0),("font",0)])

#id : tcp stream id,connect,count : number of objects downloaded for each connection, size:size of objects downloaded,start: start time of connection
#end: end time of connection, active: active time of connection, recv:total receive time of connection,send,wait,'max_size':bs+hs,'max_time':entry['timings']['receive']}
tcp_conn = []

url_map = []
object_tree = []
download_tree = []
serv_map = []
domain_map =[]
url_count = 0

#dictionary of key-domain name, value -([(connection id,dns,...)],parallel)
dns_lookups = {}

page_start = (dateutil.parser.parse(entries[0]['startedDateTime']).replace(tzinfo=None) - datetime.datetime(1970,1,1)).total_seconds()*1000
page_end = 0
load_time = parsed['log']['pages'][0]['pageTimings']['onLoad']
# entries in sorted order of startedDateTime

#total entries
count=-1
#page load time - effective wait time
opt_time1 = 0

for entry in entries:
	count+=1
	resp = entry['response'] 
	print("count -%d"%count)
	try:
		serv_addr = entry['serverIPAddress']
		#serv_map
		match_addr = False
		for x in serv_map:
  			if(x==serv_addr):
   				match_addr = True
   				break
		if(not(match_addr)):
			serv_map.append(serv_addr)
	except:
		print("server not found")
		pass	

	connection_id = 0
	url = entry['request']['url']
	
	for x in url_map:
		if(x==url):
			print("yes")

	domain_ = domain(url)
	path_ = extractPath(url) 
	s_time = (dateutil.parser.parse(entry['startedDateTime']).replace(tzinfo=None) - datetime.datetime(1970,1,1)).total_seconds()*1000
	e_time = s_time+entry['time']
 	
	#domain map
	match_dom = False
	for x in domain_map:
		if(x==domain_):
   			match_dom=True
   			break
	if(not(match_dom)):
		domain_map.append(domain_)	
	cap = pyshark.FileCapture(sys.argv[2],display_filter='http && http.request.full_uri=="'+url+'"')
	cap.apply_on_packets(print_conversation_header, timeout=100)
	print("length -%d"%(len(tcp_map)))
	for x in tcp_map:
		if(x['url']==url):
			connection_id = x['stream']
			break		

	hs=0;bs=0;obj=0;
	parent_id=0

	
	if(resp['status']==200 and resp['statusText']=='OK'):
		
		#object tree
		if(url_count!=0):
			for x in entry['request']['headers']:
				if(x['name']=='Referer'):
					referer = x['value']		#referer header
					break
			#find parent id
			for x in url_map:
				if(x[0]==referer):
					parent_id=x[1]
	
		dic1 = 	{'index':url_count,'url':url,'parent':parent_id,'count':count}		
		# json_dic1 = json.dumps(json_dic1)
		object_tree.append(dic1)
		url_map.append((url,url_count))
		url_count+=1

		#increment downloaded objects
		obj=1
		
		#size of downloaded objects
		if(resp['headersSize']!=-1):
			hs=resp['headersSize']
		if(resp['bodySize']!=-1):
			bs+=resp['bodySize']	

		#content types
		match_type = False
		for key in content_types :
			if(key in resp['content']['mimeType']):
				match_type = True
				content_types[key] += 1 
				break
		if(not(match_type)):
			content_types[resp['content']['mimeType'] ] = 1		

	match_conn=False		
	for x in tcp_conn:
		if(x['id'] ==connection_id):
			match_conn=True
			x['count']+=obj
			x['size']+= bs+hs
			
			#pipelining condition
			if(s_time<x['last']):
				print("pipelining")
			# if(x['last']>e_time):
				# print("what is this??")
			# else:
			if(x['last']<e_time):
				x['last'] = e_time	
			#end time should be end time of last object 
			if(page_end<e_time):
				page_end = e_time
			act = entry['timings']['wait']+entry['timings']['receive']+entry['timings']['send']
			x['active']+=act
			x['recv']+=entry['timings']['receive']
			x['wait']+=entry['timings']['wait']
			x['send']+=entry['timings']['send']
			if(e_time>x['end']):
				x['end'] = e_time
			if(bs+hs>x['max_size']):
				x['max_size'] = bs+hs
				x['max_time'] = entry['timings']['receive']	
			break
	if(not(match_conn)):
		page_end = e_time
		act = entry['timings']['wait']+entry['timings']['receive']+entry['timings']['send']
		d = {'id': connection_id,'connect':entry['timings']['connect'],'count':obj,'size':bs+hs,'start': s_time,'end': e_time,'active':act,
				'recv':entry['timings']['receive'],'send':entry['timings']['send'],'wait':entry['timings']['wait'],'max_size':bs+hs,
				'max_time':entry['timings']['receive'],'last':e_time}		
		tcp_conn.append(d)
		#wait,receive,send
		tmp = (connection_id,entry['timings']['dns'],entry['timings']['connect'],entry['timings']['wait'],entry['timings']['receive'],entry['timings']['send'])
		if domain_ in dns_lookups:
			dns_lookups[domain_]['conn'].append(tmp)
			dns_lookups[domain_]['total'] +=1 
		else:
			dns_lookups[domain_] = {'conn':[tmp],'parallel':1,'total':1}		
	
	#download tree
	dic  = {'domain':domain_,'conn':connection_id,'url':url}
	download_tree.append(dic)

	#update params
	header_size+=hs
	body_size+=bs
	obj_down+=obj
	del tcp_map[:]

total_size = header_size+body_size
Load_page = page_end-page_start

avg_goodput = total_size/float(load_time)
max_goodput =0

for x in tcp_conn:
	# cap = pyshark.FileCapture(sys.argv[2],display_filter="(tcp.stream eq "+str(x['id'])+")")
	# count_=-1
	# for y in cap:
	# 	count_+=1
	# print("count -%d"%count_)	
	# x['start'] = float(cap[0].frame_info.time_delta)
	# x['end'] = float(cap[count_].frame_info.time_delta)
	d = x['end']-x['start']
	# print(d)
	x['actp'] = (x['active'])/d
	x['idlep'] = 1-x['actp']
	if(x['recv']!=0):
		x['avg_g'] = (x['size'])/float(x['recv'])
	else:
		x['avg_g'] = 0
	if(x['max_time']!=0):
		x['max_g'] = (x['max_size'])/float(x['max_time'])
		if(x['max_g']>max_goodput):
			max_goodput=x['max_g']		
	else:
		x['max_g'] = 0		

#find parallel connections
#tcp_conn already sorted based on starting times
# for x in tcp_conn:
# 	print(x['start'])

#structure- list of dicts(id,free)
allotted = []
max_parallel=0	
for x in tcp_conn:
	#update allocated list
	alloc = False
	for y in allotted:
		if(y['end']<=x['start']):
			y['free']=True
		if((y['free']==True) and (not(alloc))):
			alloc = True
			y['free']=False
			y['end']=x['end']
	if(not(alloc)):
		new_entry = {'id':len(allotted),'free':False,'end':x['end']}
		allotted.append(new_entry)
	if(max_parallel<len(allotted)):
		max_parallel = len(allotted)
# print("parallel across domains -%d"%max_parallel)					
# print(avg_goodput,max_goodput)

#parallel for each domain
allotted_=[]
for x in dns_lookups:
	max_parallel_ = 0
	 
	for y in dns_lookups[x]['conn']:
		alloc_ = False
		st = 0;et = 0;
		#map id onto tcp_conn
		for i in tcp_conn:
			if(i['id']==y[0]):
				st=i['start']
				et=i['end']
				break
		if(st==0):
			print("err map")		
		for z in allotted_:
			if(z['end']<=st):
				z['free']=True
			if((z['free']==True) and (not(alloc_))):
				alloc_ = True
				z['free']=False
				z['end']=et
		if(not(alloc_)):
			new_entry_ = {'id':len(allotted_),'free':False,'end':et}
			allotted_.append(new_entry_)
		if(max_parallel_<len(allotted_)):
			max_parallel_ = len(allotted_)
	dns_lookups[x]['parallel'] = max_parallel_
	# print("parallel %s - %d"%(x,max_parallel_))
	del allotted_[:]

#4(ii)
opt_time2 = 0;
for x in dns_lookups:
	rep_g = 0 							#representative goodput for domain
	recv_size_ = 0
	wait_ = 0
	conn_ = 0
	dns_ = 0
	for y in dns_lookups[x]['conn']:
		#map with tcp conn
		for z in tcp_conn:
			if(z['id']==y[0]):
				recv_size_+=z['size']
				wait_+=z['wait']
				if(conn_==0):
					conn_= z['connect']
					dns_= y[1]
				if(z['max_time']==0):
					break
				if(z['max_g']>rep_g):
					rep_g=z['max_g']
				break
	if(rep_g!=0):			
		opt_time2+= dns_+conn_+wait_+ recv_size_/rep_g
print(opt_time2)		 			

# write to file
dt_file = "dt_"+sys.argv[1]
obj_file = "obj_"+sys.argv[1]
data_file = "data_"+sys.argv[1]
data_file2 = "domain_"+sys.argv[1]
data_file3 = "tcp_"+sys.argv[1]
#data analysis file
Datafile = open(data_file,"w")
Datafile.write("Page Load Time -%d\n\n"%Load_page)

Datafile.write("DNS LOOKUP TIME:\n")
index=0
for x in dns_lookups:
	index+=1
	c = dns_lookups[x]['conn']
	d = c[0][1]
	Datafile.write("  %d) 	id - %s\n 	total connections-%d\n	 max. parallel connections-%d\n 	dns- %d\n"%(index,x,dns_lookups[x]['total'],dns_lookups[x]['parallel'],d))
Datafile.write("\n")
Datafile.write("Maximum parallel connections opened across network -%d\n" %(max_parallel))
Datafile.write("Maximum goodput -%d\n" %(max_goodput))
Datafile.write("Average goodput -%d\n" %(avg_goodput))
index=0
Datafile.write("TCP CONNECTION ANALYSIS:\n")
for x in tcp_conn:
	index+=1
	Datafile.write('''%d) id-%s\n 	#Objects downloaded-%d\n 	Size of objects downloaded-%d 	
	Start Time-%d\n 	End Time-%d\n 	Active-%d\n 	Recieve-%d\n 	Send-%d\n 	Wait-%d\n''' 	
	%(index,x['id'],x['count'],x['size'],x['start'],x['end'],x['active'],x['recv'],x['send'],x['wait']))
	Datafile.write('''	Maximum Size of downloaded Object-'{0}'\n 	Time to download above-'{1}'\n 	Act - '{2}' 	
	Idle% - '{3}'\n 	Average Goodput - '{4}'\n 	Maximum Goodput - '{5}'\n'''.format(x['max_size'],x['max_time']
	,x['actp'],x['idlep'],x['avg_g'],x['max_g']))

Datafile.write("Page load time for second optimization -'{0}'\n ".format(opt_time2))

with open(data_file2, "w") as outfile:
	json.dump(dns_lookups, outfile, indent=4)

with open(data_file3, "w") as outfile:
	json.dump(tcp_conn, outfile, indent=4)
#download tree
with open(dt_file, "w") as outfile:
	json.dump(download_tree, outfile, indent=4)

with open(obj_file, "w") as outfile:
	json.dump(object_tree, outfile, indent=4)


